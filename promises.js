var Promise = require("bluebird");
var fs = Promise.promisifyAll(require("fs"));

function readJSON(filename) {
	return Promise.try(function(){
		return fs.readFileAsync(filename);
	}).then(function(file){
		return JSON.parse(file);
	})
}

Promise.try(function(){
	return readJSON("./sample.json");
}).then(function(parsedFile){
	console.log(parsedFile);
}).catch(function(err){
	console.log("It broke!", err);
})