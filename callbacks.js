var fs = require("fs");

function readJSON(filename, callback) {
	fs.readFile(filename, function(err, file) {
		if (err != null) {
			return callback(err);
		} else {
			var parsedFile = JSON.parse(file);
			return callback(null, parsedFile)
		}
	})
}

readJSON("./sample.json", function(err, parsedFile) {
	if (err != null) {
		console.log("It broke!", err);
	} else {
		console.log(parsedFile);
	}
})